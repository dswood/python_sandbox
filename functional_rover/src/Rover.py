'''
Created on Feb 6, 2011

@author: dwood
'''

if __name__ == '__main__':
    directions = "NESW"
    moves = [(0,1),(1,0),(0,-1),(-1,0)]
    
    inX,inY = raw_input().split()
    maxX = int(inX)
    maxY = int(inY)

    r = lambda x, y, a: (x,y,(a+1) % 4)
    l = lambda x, y, a: (x,y,(a - 1 + 4) % 4)
    def m(x, y, a):
        newX = x+moves[a][0]
        newY = y+moves[a][1] 
        if newX > maxX or newX < 0 or newY > maxY or newY < 0:
            return (x,y,a)
        else:
            return (newX,newY,a)
    
    while 1:
        x,y,dir = raw_input().split()
    
        pos = (int(x),int(y),directions.find(dir))
    
        instr = raw_input().lower()
    
        for i in instr:
            pos = locals()[i](pos[0],pos[1],pos[2]) 
            #pos = eval('%s%s' % (i, pos))
        
        print pos[0], pos[1], directions[pos[2]]
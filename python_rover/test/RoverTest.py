'''
Created on Feb 5, 2011

@author: dwood
'''
import unittest
from Grid import Grid
from Rover import *

class Test(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testMoveNorth(self):
        grid = Grid(1,2)
        rover = self.makeRover(grid)
        rover.move()
        self.assertRover(grid, rover,{'y':1})

    def makeRover(self, grid):
        return Rover({'grid':grid, 'x':0, 'y':0, 'direction':'N'})

    def assertRover(self, grid, rover, attributes={}):
        expected = Rover({'grid':grid, 'x':0, 'y':0, 'direction':'N'})
        for key,value in attributes.items():
            expected.__dict__[key] = value
        return self.assertEquals(expected, rover)

    def testRoverDoesNotMovePastEdgeOfGrid(self):
        grid = Grid(1,2)
        rover = self.makeRover(grid)
        rover.move()
        rover.move()
        self.assertRover(grid,rover,{'y':1})
        rover.direction = South
        rover.move()
        rover.move()
        self.assertRover(grid,rover,{'direction':South})
        
    def testRoverMovesEast(self):
        grid = Grid(2,2)
        rover = self.makeRover(grid)
        rover.direction = East
        rover.move()
        self.assertRover(grid,rover,{'x':1,'y':0,'direction':East})
    
    def testRoverMovesSouth(self):
        grid = Grid(2,2)
        rover = self.makeRover(grid)
        rover.direction = South
        rover.y = 1;
        rover.move()
        self.assertRover(grid,rover,{'direction':South})
        
    def testRoverMovesWest(self):
        grid = Grid(2,2)
        rover = self.makeRover(grid)
        rover.x = 1
        rover.direction = West
        rover.move()
        self.assertRover(grid,rover,{'direction':West})
        
    def testRoverTurnsRight(self):
        grid = Grid(2,2)
        rover = self.makeRover(grid)
        rover.turnRight()
        self.assertRover(grid,rover,{'direction':East})
        
    def testRoverTurnsLeft(self):
        grid = Grid(2,2)
        rover = self.makeRover(grid)
        rover.turnLeft()
        self.assertRover(grid,rover,{'direction':West})

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
'''
Created on Feb 5, 2011

@author: dwood
'''
import unittest
from Grid import Grid

class Test(unittest.TestCase):
    def testIsOutOfBounds(self):
        grid = Grid(2,2)
        self.assertTrue(grid.inBounds(0,0))
        self.assertFalse(grid.inBounds(3,0))
        self.assertFalse(grid.inBounds(0,3))
        self.assertFalse(grid.inBounds(-1,0))
        self.assertFalse(grid.inBounds(0,-1))
    
if __name__ == "__main__":
    unittest.main()
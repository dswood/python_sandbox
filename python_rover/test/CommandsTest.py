'''
Created on Feb 6, 2011

@author: dwood
'''
import unittest
import Grid
from Commands import Commands
from Rover import Rover
from Rover import North

class Test(unittest.TestCase):
    
    def testCanCreateGrid(self):
        expected = Grid.Grid(5,5)
        self.assertEquals(expected,Commands().makeGrid("5 5"))
        
    def testCanCreateRover(self):
        commands = Commands()
        grid = commands.makeGrid("5 5")
        expected = Rover({'direction':'N','x':1,'y':2,'grid':grid})
        rover = commands.makeRover("1 2 N")
        self.assertEquals(expected,rover)
        
    def testCanMoveRover(self):
        commands = Commands()
        grid = commands.makeGrid("5 5")
        expected = Rover({'direction':'N','x':1,'y':3,'grid':grid})
        commands.makeRover("1 2 N")
        rover = commands.moveRover("LMLMLMLMM")
        self.assertEquals(expected,rover)
        

if __name__ == "__main__":
    unittest.main()
'''
Created on Feb 6, 2011

@author: dwood
'''
import unittest

import RoverTest
import GridTest
import CommandsTest

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(RoverTest.Test))
    suite.addTest(unittest.makeSuite(GridTest.Test))
    suite.addTest(unittest.makeSuite(CommandsTest.Test))
    
    unittest.TextTestRunner(verbosity=2).run(suite)
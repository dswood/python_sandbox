'''
Created on Feb 5, 2011

@author: dwood
'''

class Grid:
    def __init__(self,maxX,maxY):
        self.maxX = maxX
        self.maxY = maxY
        
    def inBounds(self,x,y):
        return self.maxX > x and self.maxY > y and x >= 0 and y >= 0
    
    def __cmp__(self,other):
        if self.maxX == other.maxX:
            return self.maxY.__cmp__(other.maxY)
        return self.maxX.__cmp__(other.maxX)
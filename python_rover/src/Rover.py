'''
Created on Feb 5, 2011

@author: dwood
'''

class Rover:

    def __init__(self,args):
        directions = {'N':North,'E':East,'S':South,'W':West}
        for key,value in args.items():
            if key == 'direction':
                self.__dict__[key] = directions[value]
            else:
                self.__dict__[key] = value

    def move(self):
        if self.grid.inBounds(self.direction.moveX(self.x),
                              self.direction.moveY(self.y)):
            self.x = self.direction.moveX(self.x)
            self.y = self.direction.moveY(self.y)
    
    def turnRight(self):
        self.direction = self.direction.right()
            
    def turnLeft(self):
        self.direction = self.direction.left()
        
        
    def __cmp__(self,other):
        if other.__dict__ == self.__dict__:
            return 0
        elif other.__dict__ < self.__dict__:
                return 1;
        else:
            return -1;
    
    def __repr__(self):
        return self.__dict__.__repr__()
    
class North:
    compass = 'N'
    @staticmethod
    def left():
        return West
    @staticmethod
    def right():
        return East
    @staticmethod
    def moveX(x):
        return x
    @staticmethod
    def moveY(y):
        return y+1

class West:
    compass = 'W'
    @staticmethod
    def left():
        return South
    @staticmethod
    def right():
        return North
    @staticmethod
    def moveX(x):
        return x-1
    @staticmethod
    def moveY(y):
        return y

class South:
    compass = 'S'
    @staticmethod
    def left():
        return East
    @staticmethod
    def right():
        return West
    @staticmethod
    def moveX(x):
        return x
    @staticmethod
    def moveY(y):
        return y-1
    
class East:
    compass = 'E'
    @staticmethod
    def left():
        return North
    @staticmethod
    def right():
        return South
    @staticmethod
    def moveX(x):
        return x+1
    @staticmethod
    def moveY(y):
        return y

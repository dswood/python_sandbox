'''
Created on Feb 6, 2011

@author: dwood
'''
import Grid
import Rover

class Commands(object):

    def __init__(self):
        pass
    
    def makeGrid(self,input):
        args = input.split()
        self.grid = Grid.Grid(int(args[0]),int(args[1])) 
        return self.grid
    
    def makeRover(self,input):
        args = input.split()
        self.rover = Rover.Rover({'direction':args[2],'x':int(args[0]),'y':int(args[1]),'grid':self.grid})
        return self.rover
    
    def moveRover(self,input):
        commands = {'M':self.rover.move,'R':self.rover.turnRight,'L':self.rover.turnLeft}
        for command in input:
            commands[command]()
        return self.rover
    
        
    